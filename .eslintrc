{
  "env": {
    "node": true,
    "es6": true,
    "es2017": true
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "prettier/@typescript-eslint"
  ],
  "parser": "@typescript-eslint/parser",
  "parserOptions": {
    "project": "tsconfig-eslint.json",
    "tsconfigRootDir": "."
  },
  "plugins": ["@typescript-eslint", "prettier"],
  "rules": {
    "@typescript-eslint/naming-convention": [
      "error",
      {
        "selector": "interface",
        "format": ["PascalCase"],
        "prefix": ["I"]
      }, {
        "selector": "typeAlias",
        "format": ["PascalCase"],
        "prefix": ["T"]
      }
    ],
    "prettier/prettier": "error",
    "@typescript-eslint/type-annotation-spacing": ["error"],
    "no-console": "warn",
    "comma-dangle": ["error", "only-multiline"],
    "camelcase": ["error", { "properties": "always" }],
    "eqeqeq": ["error", "always"],
    "max-len": ["error", { "code": 120 }],
    "no-use-before-define": "error",
    "padding-line-between-statements": ["error",
      { "blankLine": "always", "prev": "*", "next": "return" },
      { "blankLine": "always", "prev": ["const", "let", "var"], "next": "*"},
      { "blankLine": "any",    "prev": ["const", "let", "var"], "next": ["const", "let", "var"]}],
    "object-curly-spacing": ["error", "always"],
    "max-depth": ["error", 2],
    "max-statements": "error",
    "max-params": "error",
    "no-magic-numbers": ["error", {"ignore": [-1, 0, 1]}],
    "func-style": ["error", "expression"],
    "no-confusing-arrow": "error",
    "no-constant-condition": "error",
    "linebreak-style": ["error", "unix"],
    "no-extra-boolean-cast": "error",
    "no-multi-spaces": "error",
    "no-extra-semi": "error",
    "no-inner-declarations": "error",
    "no-invalid-regexp": "error",
    "max-classes-per-file": "error",
    "eol-last": ["error", "always"]
  }
}
