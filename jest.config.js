module.exports = {
  roots: ['<rootDir>/src'],
  preset: 'ts-jest',
  testEnvironment: 'node',
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$',
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node', 'd.ts'],
  coveragePathIgnorePatterns: ['<rootDir>/src/logger/'],
  collectCoverage: true,
  collectCoverageFrom: [
    '**/*.{js,ts}',
    '!**/node_modules/**'
  ],
  modulePathIgnorePatterns: ['logger.ts', 'index.ts'],
  reporters: ['default', 'jest-junit'], coverageReporters: ['text-summary', 'cobertura', 'lcov'],
  coverageThreshold: {
    '**/*': {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: -10
    }
  }
};
