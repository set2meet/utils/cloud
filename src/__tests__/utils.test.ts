import { safeParseJSON } from '../utils';
import { TTaskBody } from '../types';

test('test safeParseJSON', () => {
  const data = '{"id":"1","src":"123/13","action":"create"}';
  const result = safeParseJSON<TTaskBody>(data);

  expect(result).toEqual({ id: '1', src: '123/13', action: 'create' });
});
