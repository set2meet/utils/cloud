import Consumer from '../Consumer';
import { TQueueMessage, TQueueTask, TQueueTaskManager, TConsumerConfig } from '../types';
import { mock } from 'jest-mock-extended';

/* eslint-disable max-statements, no-magic-numbers */

describe('Consumer', () => {
  const consumerConfig: TConsumerConfig = {
    nextTaskTimeout: 100,
    searchTaskTimeout: 300,
  };

  const queueMessage: TQueueMessage = {
    id: 'id_111',
    receivedAt: 100,
    body: JSON.stringify({
      id: 'id_4544',
      src: 'root/file.mp4',
      action: 'create',
    }),
  };

  const queueTaskManagerMock = mock<TQueueTaskManager>();

  beforeEach(() => {
    jest.clearAllMocks();
    jest.useFakeTimers();
  });

  test('process messages', async (done) => {
    await Promise.resolve().then(() => jest.useFakeTimers());

    queueTaskManagerMock.get.mockResolvedValueOnce(null).mockResolvedValueOnce(queueMessage);
    queueTaskManagerMock.del.mockResolvedValue(null);

    const consumer = new Consumer(consumerConfig, queueTaskManagerMock);

    consumer.onTask(
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      async (task: TQueueTask): Promise<void> => {
        expect(task.desc).toEqual({
          id: 'id_4544',
          src: 'root/file.mp4',
          action: 'create',
        });

        await task.done();

        expect(queueTaskManagerMock.del).toBeCalledWith(queueMessage.id);

        done();
      }
    );

    await Promise.resolve().then(() => jest.advanceTimersByTime(consumerConfig.nextTaskTimeout));
    await Promise.resolve().then(() => jest.advanceTimersByTime(consumerConfig.searchTaskTimeout));
    await Promise.resolve().then(() => jest.advanceTimersByTime(consumerConfig.searchTaskTimeout));
  });

  test('process messages with error', async (done) => {
    await Promise.resolve().then(() => jest.useFakeTimers());

    queueTaskManagerMock.get.mockResolvedValueOnce(null).mockResolvedValueOnce(queueMessage);
    queueTaskManagerMock.del.mockResolvedValue(null);
    jest.spyOn(Date, 'now').mockReturnValue(2000);

    const consumer = new Consumer(consumerConfig, queueTaskManagerMock);

    consumer.onTask(
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      async (task: TQueueTask) => {
        expect(task.desc).toEqual({
          id: 'id_4544',
          src: 'root/file.mp4',
          action: 'create',
        });

        await task.break();

        expect(queueTaskManagerMock.wait).toBeCalledWith('id_111', 602);

        done();
      }
    );

    await Promise.resolve().then(() => jest.advanceTimersByTime(consumerConfig.nextTaskTimeout));
    await Promise.resolve().then(() => jest.advanceTimersByTime(consumerConfig.searchTaskTimeout));
    await Promise.resolve().then(() => jest.advanceTimersByTime(consumerConfig.searchTaskTimeout));
  });
});
