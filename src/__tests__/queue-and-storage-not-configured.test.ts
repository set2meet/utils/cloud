import getQueue from '../getQueue';
import getStorage from '../getStorage';

jest.mock('../logger/logger');
jest.mock('../aws/sqsConfig', () => null);
jest.mock('../aws/SQSQueue');
jest.mock('../Consumer');
jest.mock('../aws/s3cConfig', () => null);
jest.mock('../aws/S3Storage');
jest.mock('../utils');

test('queue-service not configured', () => {
  expect(() => {
    getQueue();
  }).toThrowError('Cloud Queue is not configured.');
});

test('storage-service not configured', () => {
  expect(() => {
    getStorage();
  }).toThrowError('Cloud Storage is not configured.');
});
