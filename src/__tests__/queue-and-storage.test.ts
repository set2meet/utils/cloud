import getQueue from '../getQueue';
import getStorage from '../getStorage';

jest.mock('../utils');

jest.mock('../aws/sqsConfig', () => {
  return {};
});

jest.mock('../aws/s3cConfig', () => {
  return {};
});

jest.mock('../aws/SQSQueue', () => {
  return jest.fn().mockImplementation(() => {
    return {
      default: jest.fn(),
    };
  });
});

jest.mock('../aws/S3Storage', () => {
  return jest.fn().mockImplementation(() => {
    return {
      default: jest.fn(),
    };
  });
});

test('queue configured', () => {
  expect(getQueue()).not.toBeNull();
});

test('storage configured', () => {
  expect(getStorage()).not.toBeNull();
});
