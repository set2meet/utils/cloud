/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { DummyLogger, ILogger } from '@s2m/logger';
import { ELogErrorCode } from './ELogErrorCode';
import { ELogModule } from './ELogModule';

interface ICumulativeLogger {
  sqs: ILogger<ELogModule, ELogErrorCode>;
  s3: ILogger<ELogModule, ELogErrorCode>;
}

let cumulativeLogger: ICumulativeLogger;

let parentLogger: DummyLogger<ELogModule, ELogErrorCode> = new DummyLogger();

const initLoggers = (logger: ILogger<ELogModule, ELogErrorCode>) => {
  cumulativeLogger = {
    sqs: logger.createBoundChild(ELogModule.Sqs),
    s3: logger.createBoundChild(ELogModule.S3),
  };
};

initLoggers(parentLogger);

export const registerLogger = (logger: ILogger<ELogModule, ELogErrorCode>): void => {
  parentLogger = logger;

  initLoggers(parentLogger);
};

export const getLogger = (): ICumulativeLogger => {
  return cumulativeLogger;
};
