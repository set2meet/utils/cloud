import awsSqsConfig from './aws/sqsConfig';
import SQSQueue from './aws/SQSQueue';
import { IQueue } from './types';

let queue: IQueue = null;

const getQueue = (): IQueue => {
  if (awsSqsConfig) {
    queue = new SQSQueue();
  }

  if (queue) {
    return queue;
  }

  throw new Error('Cloud Queue is not configured.');
};

export default getQueue;
