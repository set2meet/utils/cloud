export type TTaskBodyPropertyValue = number | string;

export enum TaskActionType {
  Create = 'create',
  Remove = 'remove',
}

export type TTaskListener = (task: TQueueTask) => void;

export type TConsumerConfig = {
  nextTaskTimeout: number;
  searchTaskTimeout: number;
  waitTimeout?: number;
  visibilityTimeout?: number;
};

export type TTaskBody = {
  id: string; // record id
  src: string; // record s3 key
  action: TaskActionType; // action to do / done
  [key: string]: TTaskBodyPropertyValue;
};

export type TQueueTask = {
  desc: TTaskBody;
  done: () => Promise<void>;
  break: () => Promise<void>;
};

export interface IConsumer {
  emitTask(taskBody: TTaskBody): void;
  onTask(listener: TTaskListener): void;
}

export interface IQueue {
  createRecordTask(task: TTaskBody): Promise<void>;
  createRecordReport(task: TTaskBody): Promise<void>;
  getConsumerForTask(consumerConfig: TConsumerConfig): IConsumer;
  getConsumerForReport(consumerConfig: TConsumerConfig): IConsumer;
}

export enum S3Target {
  DRAFT = 'draft',
  FINAL = 'final',
}

export type TTaskDescription = {
  id: string; // sqs message id
  body: TTaskBody; // recording task info
  receivedAt: number; // lifetime of sqs message in seconds
};

export type TQueueMessage = {
  id: string;
  body: string;
  receivedAt: number; // seconds
};

export type TQueueTaskGet = (visibilityTimeoutSeconds: number) => Promise<TQueueMessage>;

export type TQueueTaskDel = (id: string) => Promise<void>;

export type TQueueTaskWait = (id: string, seconds: number) => Promise<void>;

export type TQueueTaskManager = {
  get: TQueueTaskGet;
  del: TQueueTaskDel;
  wait: TQueueTaskWait;
};

export type TMetadata = {
  type?: string;
};

export interface IStorage {
  uploadRecordingCaptureVideos(id: string, src: string): Promise<string>;
  uploadRecordingProcessingVideo(src: string, metadata?: TMetadata): Promise<string>;
  signedUrl(key: string): Promise<string>;

  removeFromS3Dir(src: string): Promise<void>;
  removeFromS3File(src: string): Promise<void>;
  downloadFromS3Dir(src: string, dst: string): Promise<void>;
}
