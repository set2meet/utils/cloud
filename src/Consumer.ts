import { EventEmitter } from 'events';
import { safeParseJSON } from './utils';
import { TConsumerConfig, TTaskBody, TTaskDescription, TQueueTaskManager, TTaskListener, IConsumer } from './types';

const oneSec = 1000;
const brokenTaskTimeout = 600; // 10 min in seconds
const defaultVisibilityTimeout = 600; // 10 min in seconds

export default class Consumer extends EventEmitter implements IConsumer {
  private queueTaskManager: TQueueTaskManager;
  private taskDescription: TTaskDescription;

  private nextTaskTimeout: number;
  private searchTaskTimeout: number;
  /**
   * time on which postpone current task in sec
   */
  private waitTimeout: number;
  /**
   * time on which current task will be invisible for consumers in sec
   */
  private visibilityTimeout: number; //sec

  constructor(config: TConsumerConfig, queueTaskManager: TQueueTaskManager) {
    super();

    this.queueTaskManager = queueTaskManager;
    this.nextTaskTimeout = config.nextTaskTimeout;
    this.searchTaskTimeout = config.searchTaskTimeout;
    this.waitTimeout = config.waitTimeout || brokenTaskTimeout;
    this.visibilityTimeout = config.visibilityTimeout || defaultVisibilityTimeout;

    this.nextTask();
  }

  private nextTask = () => {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    setTimeout(this.searchNewTask, this.nextTaskTimeout);
  };

  private taskBreak = async () => {
    await this.terminateTaskTimeout();

    this.nextTask();
  };

  private taskDone = async () => {
    await this.queueTaskManager.del(this.taskDescription.id);

    this.nextTask();
  };

  emitTask(taskBody: TTaskBody): void {
    this.emit('task', {
      desc: taskBody,
      done: this.taskDone,
      break: this.taskBreak,
    });
  }

  onTask(listener: TTaskListener): void {
    this.on('task', listener);
  }

  private searchNewTask = async (): Promise<void> => {
    this.taskDescription = await this.getNewTaskDescription();

    if (this.taskDescription) {
      this.emitTask(this.taskDescription.body);
    } else {
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      setTimeout(this.searchNewTask, this.searchTaskTimeout);
    }
  };

  private getNewTaskDescription = async (): Promise<TTaskDescription> => {
    const message = await this.queueTaskManager.get(this.visibilityTimeout);

    if (!message) {
      return null;
    }

    const body = safeParseJSON<TTaskBody>(message.body);

    return { ...message, body };
  };

  private async terminateTaskTimeout() {
    const lifetime = Math.round((Date.now() - this.taskDescription.receivedAt) / oneSec);

    await this.queueTaskManager.wait(this.taskDescription.id, lifetime + this.waitTimeout);
  }
}
