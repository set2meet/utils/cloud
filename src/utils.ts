export const safeParseJSON = <T>(data: string): T => {
  return JSON.parse(data) as T;
};
