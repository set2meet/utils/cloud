import path from 'path';
import fs from 'fs-extra';
import AWS, { S3 } from 'aws-sdk';
import config from './s3cConfig';
import dateFormat from 'dateformat';
import { TMetadata, IStorage, S3Target } from '../types';
import { getLogger } from '../logger/logger';
import { ELogErrorCode } from '../logger/ELogErrorCode';

type TUploadFile = { src: string; dst: string; metadata?: TMetadata };
type TMakeUploadFile = (name: string) => TUploadFile;
type TAWSSendData = AWS.S3.ManagedUpload.SendData;

const dateFormatPath = () => dateFormat('yyyy/mm/dd');
const dateFormatPrefix = () => dateFormat('yyyy-mm-dd');

const getDestDir = (id: string, target: S3Target) => {
  return `${config.destination.target[target]}/${dateFormatPrefix()}...${id}`;
};

const buildMapFullPath = (dst: string, src: string, metadata?: TMetadata): TMakeUploadFile => {
  return (name: string) => ({
    src: path.resolve(src, name),
    dst: `${dst}/${name}`,
    metadata,
  });
};

export default class S3Storage implements IStorage {
  private downloadFile = (dst: string) => (src: string) => {
    return new Promise<void>((resolve, reject) => {
      const dstFilePath = path.resolve(dst, path.basename(src));
      const writeStream = fs.createWriteStream(dstFilePath);

      getLogger().s3.info('downloadFile start', { src, dst: dstFilePath });

      const s3stream = this.s3bucket
        .getObject({
          Bucket: config.destination.bucket,
          Key: src,
        })
        .createReadStream();

      // listen for errors returned by the service
      s3stream.on('error', (err) => {
        getLogger().s3.error(ELogErrorCode.S3Operation, 'downloadFile read stream error', { message: err.message });
        reject(err);
      });

      // pipe s3 read stream to local path
      s3stream
        .pipe(writeStream)
        .on('error', (err) => {
          getLogger().s3.error(ELogErrorCode.S3Operation, 'downloadFile write stream error', { message: err.message });
          getLogger().s3.info('downloadFile start', { src, dst: dstFilePath });
          reject(err);
        })
        .on('finish', () => {
          getLogger().s3.info('downloadFile finish');
          resolve();
        });
    });
  };

  private downloadListOfFiles = async (dst: string, list: string[]) => {
    return await Promise.all(list.map(this.downloadFile(dst)));
  };

  private removeFiles = async (files: string[]): Promise<void> => {
    getLogger().s3.info('removeFiles', { files });

    const params = {
      Bucket: config.destination.bucket,
      Delete: {
        Quiet: false,
        Objects: files.map((Key) => ({ Key })),
      },
    };

    await this.s3bucket.deleteObjects(params).promise();
  };

  private getListOfFiles = async (src: string): Promise<string[]> => {
    getLogger().s3.info('getListOfFiles', { src });

    const data = await this.s3bucket
      .listObjects({
        Bucket: config.destination.bucket,
        Prefix: src,
      })
      .promise();

    return data.Contents.map((item) => item.Key);
  };

  private uploadFile = (file: TUploadFile): Promise<TAWSSendData> => {
    const readStream = fs.createReadStream(file.src);
    const params: AWS.S3.PutObjectRequest = {
      Bucket: config.destination.bucket,
      Body: readStream,
      Key: file.dst,
      Metadata: file.metadata,
    };

    getLogger().s3.info('uploadFile', { file });

    return this.s3bucket.upload(params).promise();
  };

  public async uploadRecordingCaptureVideos(id: string, src: string, metadata?: TMetadata): Promise<string> {
    const dst = getDestDir(id, S3Target.DRAFT);
    const mapToFileUpload = buildMapFullPath(dst, src, metadata);
    const items = await fs.readdir(src);

    getLogger().s3.info('uploadRecordingCaptureVideos', {
      src,
      dst,
      count: items.length,
    });

    if (items.length) {
      const files = items.map(mapToFileUpload);

      await Promise.all(files.map(this.uploadFile));
    }

    return dst;
  }

  private s3bucket: S3;

  constructor() {
    const endpoint = config.destination.endpoint;

    this.s3bucket = new AWS.S3({
      endpoint,
      s3ForcePathStyle: !!endpoint,
      accessKeyId: config.credentials.key,
      secretAccessKey: config.credentials.secret,
      maxRetries: config.maxRetries,
    });
  }

  public async signedUrl(key: string): Promise<string> {
    const signedUrlExpireSeconds = 300;
    const params = {
      Bucket: config.destination.bucket,
      Key: key,
    };

    await this.s3bucket.headObject(params).promise();

    return await this.s3bucket.getSignedUrlPromise('getObject', {
      ...params,
      Expires: signedUrlExpireSeconds,
    });
  }

  public async uploadRecordingProcessingVideo(src: string, metadata?: TMetadata): Promise<string> {
    const filename = path.basename(src);
    const dst = `${config.destination.target[S3Target.FINAL]}/${dateFormatPath()}/${filename}`;

    getLogger().s3.info('uploadRecordingProcessingVideo', { src, dst });
    await this.uploadFile({ src, dst, metadata });

    return dst;
  }

  public async removeFromS3Dir(src: string): Promise<void> {
    getLogger().s3.info('removeFromS3Dir', { src });

    const list = await this.getListOfFiles(src);

    await this.removeFiles(list);
  }

  public async removeFromS3File(src: string): Promise<void> {
    getLogger().s3.info('removeFromS3File', { src });

    await this.removeFiles([src]);
  }

  public async downloadFromS3Dir(src: string, dst: string): Promise<void> {
    getLogger().s3.info('downloadFromS3Dir', { src, dst });

    await fs.ensureDir(dst);

    const list = await this.getListOfFiles(src);

    await this.downloadListOfFiles(dst, list);
  }
}
