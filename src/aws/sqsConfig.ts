import cfg from 'config';

const baseKey = 'sqs';

export type TSQSConfig = {
  credentials: {
    key: string;
    secret: string;
  };
  maxRetries?: number;
  destination: {
    group: string;
    region: string;
    queue: {
      task: string;
      report: string;
    };
  };
};

const configExists =
  cfg.has(baseKey) &&
  cfg.has(`${baseKey}.credentials`) &&
  cfg.has(`${baseKey}.credentials.key`) &&
  cfg.has(`${baseKey}.credentials.secret`) &&
  cfg.has(`${baseKey}.destination`) &&
  cfg.has(`${baseKey}.destination.group`) &&
  cfg.has(`${baseKey}.destination.region`) &&
  cfg.has(`${baseKey}.destination.queue`) &&
  cfg.has(`${baseKey}.destination.queue.task`) &&
  cfg.has(`${baseKey}.destination.queue.report`);

const config: TSQSConfig = configExists
  ? {
      credentials: {
        key: cfg.get(`${baseKey}.credentials.key`),
        secret: cfg.get(`${baseKey}.credentials.secret`),
      },
      destination: {
        group: cfg.get(`${baseKey}.destination.group`),
        region: cfg.get(`${baseKey}.destination.region`),
        queue: {
          task: cfg.get(`${baseKey}.destination.queue.task`),
          report: cfg.get(`${baseKey}.destination.queue.report`),
        },
      },
    }
  : null;

if (config && cfg.has(`${baseKey}.maxRetries`)) {
  config.maxRetries = cfg.get(`${baseKey}.maxRetries`);
}

export default config;
