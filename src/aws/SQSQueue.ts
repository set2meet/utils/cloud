import uuid from 'uuid';
import AWS from 'aws-sdk';
import config from './sqsConfig';
import Consumer from '../Consumer';
import { TConsumerConfig, TQueueMessage, TTaskBody, IQueue } from '../types';
import SQS = require('aws-sdk/clients/sqs');
import { getLogger } from '../logger/logger';

const isFifoQueue = (url: string) => url.endsWith('.fifo');

export default class SQSQueue implements IQueue {
  private sqs: SQS;

  private sendMessage = async (url: string, task: TTaskBody): Promise<void> => {
    getLogger().sqs.info('Send message', { url, task });

    const specificParams = isFifoQueue(url)
      ? {
          MessageGroupId: config.destination.group,
          MessageDeduplicationId: uuid(),
        }
      : {};

    await this.sqs
      .sendMessage({
        QueueUrl: url,
        MessageBody: JSON.stringify(task),
        ...specificParams,
      })
      .promise();
  };

  private receiveMessage = async (url: string, visibilityTimeout: number): Promise<TQueueMessage> => {
    const resp = await this.sqs
      .receiveMessage({
        QueueUrl: url,
        MaxNumberOfMessages: 1,
        VisibilityTimeout: visibilityTimeout,
      })
      .promise();
    const messages = resp.Messages || [];
    const message = messages[0];

    if (!message) {
      return null;
    }

    getLogger().sqs.info('Receive message', { url, message });

    return {
      id: message.ReceiptHandle,
      body: message.Body,
      receivedAt: Date.now(),
    };
  };

  private removeMessage = async (url: string, id: string): Promise<void> => {
    getLogger().sqs.info('Remove message', { url, id });

    await this.sqs
      .deleteMessage({
        QueueUrl: url,
        ReceiptHandle: id,
      })
      .promise();
  };

  private changeMessageVisibility = async (url: string, id: string, lifetime: number): Promise<void> => {
    getLogger().sqs.info('Change message visibility', { url, id, lifetime });

    await this.sqs
      .changeMessageVisibility({
        QueueUrl: url,
        ReceiptHandle: id,
        VisibilityTimeout: lifetime,
      })
      .promise();
  };

  constructor() {
    this.sqs = new AWS.SQS({
      region: config.destination.region,
      accessKeyId: config.credentials.key,
      secretAccessKey: config.credentials.secret,
      maxRetries: config.maxRetries,
    });
  }

  createRecordTask(task: TTaskBody): Promise<void> {
    const taskUrl = config.destination.queue.task;

    return this.sendMessage(taskUrl, task);
  }

  createRecordReport(task: TTaskBody): Promise<void> {
    const reportUrl = config.destination.queue.report;

    return this.sendMessage(reportUrl, task);
  }

  getConsumerForTask(consumerConfig: TConsumerConfig): Consumer {
    const taskUrl = config.destination.queue.task;

    return new Consumer(consumerConfig, {
      get: (visibilityTimeout: number): Promise<TQueueMessage> => this.receiveMessage(taskUrl, visibilityTimeout),
      del: (id: string): Promise<void> => this.removeMessage(taskUrl, id),
      wait: (id: string, lifetime: number) => this.changeMessageVisibility(taskUrl, id, lifetime),
    });
  }

  getConsumerForReport(consumerConfig: TConsumerConfig): Consumer {
    const reportUrl = config.destination.queue.report;

    return new Consumer(consumerConfig, {
      get: (visibilityTimeout: number): Promise<TQueueMessage> => this.receiveMessage(reportUrl, visibilityTimeout),
      del: (id: string): Promise<void> => this.removeMessage(reportUrl, id),
      wait: (id: string, lifetime: number) => this.changeMessageVisibility(reportUrl, id, lifetime),
    });
  }
}
