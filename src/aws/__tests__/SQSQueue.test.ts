import { TSQSConfig } from '../sqsConfig';
import SQSQueue from '../SQSQueue';
import { TQueueTaskManager, TTaskBody, TaskActionType } from '../../types';
import Consumer from '../../Consumer';
import { mock } from 'jest-mock-extended';
import { ILogger } from '@s2m/logger';
import { ELogModule } from '../../logger/ELogModule';
import { ELogErrorCode } from '../../logger/ELogErrorCode';

/* eslint-disable @typescript-eslint/no-unsafe-call,
@typescript-eslint/no-unsafe-member-access,
@typescript-eslint/no-unsafe-assignment,
@typescript-eslint/no-unused-vars,
@typescript-eslint/no-explicit-any,
max-statements
*/

jest.mock('../../Consumer');

jest.mock(
  '../sqsConfig',
  (): TSQSConfig => {
    return {
      credentials: {
        key: 'key',
        secret: 'secret',
      },
      destination: {
        group: 'group',
        region: 'es-region',
        queue: {
          task: 'task-url',
          report: 'report-url.fifo',
        },
      },
    };
  }
);

jest.mock(
  'uuid',
  jest.fn(() => () => 'uuid')
);

jest.doMock('../../logger/logger', () => ({
  sqs: mock<ILogger<ELogModule, ELogErrorCode>>(),
}));

const mockSQSSendMessage = jest.fn();
const mockSQSReceiveMessage = jest.fn();
const mockSQSDeleteMessage = jest.fn();
const mockSQSChangeMessageVisibility = jest.fn();

jest.mock('aws-sdk', () => {
  class SQS {
    sendMessage = mockSQSSendMessage;
    receiveMessage = mockSQSReceiveMessage;
    deleteMessage = mockSQSDeleteMessage;
    changeMessageVisibility = mockSQSChangeMessageVisibility;
  }

  return {
    SQS,
  };
});

describe('test sqs api', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const consumerConfig = { nextTaskTimeout: 100, searchTaskTimeout: 100 };

  test('createRecordTask & createRecordReport', async () => {
    mockSQSSendMessage.mockImplementation(() => ({
      promise: () => Promise.resolve(),
    }));

    const sqsQueue = new SQSQueue();
    const taskBody: TTaskBody = {
      id: 'recordId',
      src: 's3key/123.mp4',
      action: TaskActionType.Create,
    };

    await sqsQueue.createRecordReport(taskBody);

    expect(mockSQSSendMessage).toHaveBeenNthCalledWith(1, {
      MessageBody: '{"id":"recordId","src":"s3key/123.mp4","action":"create"}',
      MessageDeduplicationId: 'uuid',
      MessageGroupId: 'group',
      QueueUrl: 'report-url.fifo',
    });

    await sqsQueue.createRecordTask(taskBody);

    // eslint-disable-next-line no-magic-numbers
    expect(mockSQSSendMessage).toHaveBeenNthCalledWith(2, {
      MessageBody: '{"id":"recordId","src":"s3key/123.mp4","action":"create"}',
      QueueUrl: 'task-url',
    });
  });

  test('getConsumerForTask receiveMessage', async () => {
    const visibilityTimeout = 600;

    mockSQSReceiveMessage.mockImplementationOnce(() => ({
      promise: () =>
        Promise.resolve({
          Messages: [
            {
              ReceiptHandle: 'id_ReceiptHandle',
              Body: '{}',
            },
          ],
        }),
    }));
    mockSQSReceiveMessage.mockImplementationOnce(() => ({
      promise: () => Promise.resolve({ Messages: [] }),
    }));

    const sqsQueue = new SQSQueue();
    const consumer = sqsQueue.getConsumerForTask(consumerConfig) as any;
    const SQSConsumerConstructor = Consumer as any;

    expect(SQSConsumerConstructor.mock.calls[0][0]).toBe(consumerConfig);

    const taskManager = SQSConsumerConstructor.mock.calls[0][1] as TQueueTaskManager;
    const message = await taskManager.get(visibilityTimeout);

    expect(message.id).toBe('id_ReceiptHandle');
    expect(mockSQSReceiveMessage).toBeCalledWith({
      QueueUrl: 'task-url',
      MaxNumberOfMessages: 1,
      VisibilityTimeout: 600,
    });

    const message2 = await taskManager.get(visibilityTimeout);

    expect(message2).toBeNull();
    expect(mockSQSReceiveMessage).toBeCalledWith({
      QueueUrl: 'task-url',
      MaxNumberOfMessages: 1,
      VisibilityTimeout: 600,
    });
  });

  test('getConsumerForTask deleteMessage', async () => {
    mockSQSDeleteMessage.mockImplementation(() => ({
      promise: () => Promise.resolve(),
    }));

    const sqsQueue = new SQSQueue();

    const consumer = sqsQueue.getConsumerForTask(consumerConfig) as any;
    const SQSConsumerConstructor = Consumer as any;

    expect(SQSConsumerConstructor.mock.calls[0][0]).toBe(consumerConfig);

    const taskManager = SQSConsumerConstructor.mock.calls[0][1] as TQueueTaskManager;
    const id = '123';

    await taskManager.del(id);

    expect(mockSQSDeleteMessage).toBeCalledWith({
      QueueUrl: 'task-url',
      ReceiptHandle: id,
    });
  });

  test('getConsumerForTask changeMessageVisibility', async () => {
    mockSQSChangeMessageVisibility.mockImplementation(() => ({
      promise: () => Promise.resolve({}),
    }));

    const sqsQueue = new SQSQueue();
    const consumer = sqsQueue.getConsumerForTask(consumerConfig) as any;
    const SQSConsumerConstructor = Consumer as any;

    expect(SQSConsumerConstructor.mock.calls[0][0]).toBe(consumerConfig);

    const taskManager = SQSConsumerConstructor.mock.calls[0][1] as TQueueTaskManager;
    const id = '123';
    const seconds = 1234;

    await taskManager.wait(id, seconds);

    expect(mockSQSChangeMessageVisibility).toBeCalledWith({
      QueueUrl: 'task-url',
      ReceiptHandle: id,
      VisibilityTimeout: seconds,
    });
  });

  test('getConsumerForReport receiveMessage', async () => {
    const visibilityTimeout = 600;

    mockSQSReceiveMessage.mockImplementationOnce(() => ({
      promise: () =>
        Promise.resolve({
          Messages: [
            {
              ReceiptHandle: 'id_ReceiptHandle',
              Body: '{}',
            },
          ],
        }),
    }));
    mockSQSReceiveMessage.mockImplementationOnce(() => ({
      promise: () => Promise.resolve({ Messages: [] }),
    }));

    mockSQSReceiveMessage.mockImplementationOnce(() => ({
      promise: () => Promise.resolve({ Messages: null }),
    }));

    const sqsQueue = new SQSQueue();
    const consumer = sqsQueue.getConsumerForReport(consumerConfig) as any;
    const SQSConsumerConstructor = Consumer as any;

    expect(SQSConsumerConstructor.mock.calls[0][0]).toBe(consumerConfig);

    const taskManager = SQSConsumerConstructor.mock.calls[0][1] as TQueueTaskManager;
    const message = await taskManager.get(visibilityTimeout);

    expect(message.id).toBe('id_ReceiptHandle');
    expect(mockSQSReceiveMessage).toBeCalledWith({
      QueueUrl: 'report-url.fifo',
      MaxNumberOfMessages: 1,
      VisibilityTimeout: 600,
    });

    const message2 = await taskManager.get(visibilityTimeout);

    expect(message2).toBeNull();
    expect(mockSQSReceiveMessage).toBeCalledWith({
      QueueUrl: 'report-url.fifo',
      MaxNumberOfMessages: 1,
      VisibilityTimeout: 600,
    });

    const message3 = await taskManager.get(visibilityTimeout);

    expect(message3).toBeNull();
    expect(mockSQSReceiveMessage).toBeCalledWith({
      QueueUrl: 'report-url.fifo',
      MaxNumberOfMessages: 1,
      VisibilityTimeout: 600,
    });
  });

  test('getConsumerForReport deleteMessage', async () => {
    mockSQSDeleteMessage.mockImplementation(() => ({
      promise: () => Promise.resolve(),
    }));

    const sqsQueue = new SQSQueue();

    const consumer = sqsQueue.getConsumerForReport(consumerConfig) as any;
    const SQSConsumerConstructor = Consumer as any;

    expect(SQSConsumerConstructor.mock.calls[0][0]).toBe(consumerConfig);

    const taskManager = SQSConsumerConstructor.mock.calls[0][1] as TQueueTaskManager;
    const id = '123';

    await taskManager.del(id);

    expect(mockSQSDeleteMessage).toBeCalledWith({
      QueueUrl: 'report-url.fifo',
      ReceiptHandle: id,
    });
  });

  test('getConsumerForReport changeMessageVisibility', async () => {
    mockSQSChangeMessageVisibility.mockImplementation(() => ({
      promise: () => Promise.resolve({}),
    }));

    const sqsQueue = new SQSQueue();
    const consumer = sqsQueue.getConsumerForReport(consumerConfig) as any;
    const SQSConsumerConstructor = Consumer as any;

    expect(SQSConsumerConstructor.mock.calls[0][0]).toBe(consumerConfig);

    const taskManager = SQSConsumerConstructor.mock.calls[0][1] as TQueueTaskManager;
    const id = '123';
    const seconds = 1234;

    await taskManager.wait(id, seconds);

    expect(mockSQSChangeMessageVisibility).toBeCalledWith({
      QueueUrl: 'report-url.fifo',
      ReceiptHandle: id,
      VisibilityTimeout: seconds,
    });
  });
});
