import _get from 'lodash/get';
import sqsConfig from '../sqsConfig';

jest.mock('config', () => {
  const cfg = {
    sqs: {
      maxRetries: 1,
      credentials: {
        key: 'x',
        secret: 'x',
      },
      destination: {
        group: 'recording',
        region: 'us-east-1',
        queue: {
          task: 'http://localhost:4566/000000000000/s2m_recording_task.fifo',
          report: 'http://localhost:4566/000000000000/s2m_recording_complete.fifo',
        },
      },
    },
  };

  const has = jest.fn().mockImplementation((name: string) => !!_get(cfg, name));
  const get = jest.fn().mockImplementation((name) => _get(cfg, name) as string);

  return {
    has,
    get,
  };
});

test('test sqs config exists', () => {
  expect(sqsConfig).toBeDefined();
  expect(sqsConfig.maxRetries).toBe(1);
  expect(sqsConfig.credentials).toEqual({
    key: 'x',
    secret: 'x',
  });
  expect(sqsConfig.destination).toEqual({
    group: 'recording',
    region: 'us-east-1',
    queue: {
      task: 'http://localhost:4566/000000000000/s2m_recording_task.fifo',
      report: 'http://localhost:4566/000000000000/s2m_recording_complete.fifo',
    },
  });
});
