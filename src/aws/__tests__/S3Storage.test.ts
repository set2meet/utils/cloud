import { TS3BConfig } from '../s3cConfig';
import path from 'path';
import fs from 'fs-extra';
import { ReadStream, WriteStream } from 'fs';
import S3Storage from '../S3Storage';
import { PassThrough, Readable } from 'stream';
import { mock } from 'jest-mock-extended';
import { ILogger } from '@s2m/logger';
import { ELogModule } from '../../logger/ELogModule';
import { ELogErrorCode } from '../../logger/ELogErrorCode';

/* eslint-disable @typescript-eslint/no-explicit-any, max-statements */

jest.mock('path');
jest.mock('fs-extra');

jest.doMock('../../logger/logger', () => ({
  s3: mock<ILogger<ELogModule, ELogErrorCode>>(),
}));

const mockS3Upload = jest.fn();
const mockS3HeadObject = jest.fn();
const mockS3GetSignedUrlPromise = jest.fn();
const mockS3ListObjects = jest.fn();
const mockS3DeleteObjects = jest.fn();
const mockS3GetObject = jest.fn();

jest.mock('aws-sdk', () => {
  class S3 {
    upload = mockS3Upload;
    headObject = mockS3HeadObject;
    getSignedUrlPromise = mockS3GetSignedUrlPromise;
    listObjects = mockS3ListObjects;
    deleteObjects = mockS3DeleteObjects;
    getObject = mockS3GetObject;
  }

  return {
    S3,
  };
});

jest.mock(
  '../s3cConfig',
  (): TS3BConfig => ({
    credentials: {
      key: 'key',
      secret: 'secret',
    },
    destination: {
      bucket: 'bucket',
      target: {
        draft: 'draft',
        final: 'final',
      },
    },
  })
);

jest.mock('dateformat', () => (format: string) => format);

describe('S3Storage', () => {
  let s3: S3Storage = null;

  beforeEach(() => {
    jest.resetAllMocks();
    jest.restoreAllMocks();

    s3 = new S3Storage();
  });

  test('uploadRecordingCaptureVideos', async () => {
    const pathResolveMock = jest.spyOn(path, 'resolve').mockReturnValue('full_path');
    const fsReaddirMock = jest.spyOn(fs, 'readdir').mockReturnValue(Promise.resolve(['file1', 'file2']));

    mockS3Upload.mockImplementation(() => ({
      promise: () => Promise.resolve({}),
    }));

    jest.spyOn(fs, 'createReadStream').mockReturnValue(('file_stream' as any) as ReadStream);

    const uri = await s3.uploadRecordingCaptureVideos('id1', 'src');

    expect(fsReaddirMock).toBeCalledWith('src');
    expect(pathResolveMock).toHaveBeenNthCalledWith(1, 'src', 'file1');
    // eslint-disable-next-line no-magic-numbers
    expect(pathResolveMock).toHaveBeenNthCalledWith(2, 'src', 'file2');
    expect(mockS3Upload).toHaveBeenNthCalledWith(1, {
      Bucket: 'bucket',
      Body: 'file_stream',
      Key: 'draft/yyyy-mm-dd...id1/file1',
      Metadata: undefined,
    });
    // eslint-disable-next-line no-magic-numbers
    expect(mockS3Upload).toHaveBeenNthCalledWith(2, {
      Body: 'file_stream',
      Bucket: 'bucket',
      Key: 'draft/yyyy-mm-dd...id1/file2',
      Metadata: undefined,
    });
    expect(uri).toBe('draft/yyyy-mm-dd...id1');
  });

  test('uploadRecordingCaptureVideos, no files', async () => {
    const pathResolveMock = jest.spyOn(path, 'resolve').mockReturnValue('full_path');
    const fsReaddirMock = jest.spyOn(fs, 'readdir').mockReturnValue(Promise.resolve([]));

    mockS3Upload.mockImplementation(() => ({
      promise: () => Promise.resolve({}),
    }));

    jest.spyOn(fs, 'createReadStream').mockReturnValue(('file_stream' as any) as ReadStream);

    await s3.uploadRecordingCaptureVideos('id1', 'src');

    expect(fsReaddirMock).toBeCalledWith('src');
    expect(pathResolveMock).not.toBeCalled();
    expect(mockS3Upload).not.toBeCalled();
  });

  test('uploadRecordingProcessingVideo', async () => {
    jest.spyOn(path, 'basename').mockReturnValue('basename1');
    jest.spyOn(fs, 'createReadStream').mockReturnValue(('file_stream' as any) as ReadStream);

    mockS3Upload.mockImplementation(() => ({
      promise: () => Promise.resolve({}),
    }));

    const uri = await s3.uploadRecordingProcessingVideo('some_src');

    expect(mockS3Upload).toBeCalledWith({
      Body: 'file_stream',
      Bucket: 'bucket',
      Key: 'final/yyyy/mm/dd/basename1',
      Metadata: undefined,
    });
    expect(uri).toBe('final/yyyy/mm/dd/basename1');
  });

  test('signedUrl', async () => {
    mockS3HeadObject.mockImplementation(() => ({
      promise: () => Promise.resolve(),
    }));
    mockS3GetSignedUrlPromise.mockResolvedValue('signedUrl');

    const uri = await s3.signedUrl('key');

    expect(mockS3HeadObject).toBeCalledWith({ Bucket: 'bucket', Key: 'key' });
    expect(mockS3GetSignedUrlPromise).toBeCalledWith('getObject', {
      Bucket: 'bucket',
      Key: 'key',
      Expires: 300,
    });
    expect(uri).not.toBeNull();
    expect(uri).toBe('signedUrl');
  });

  test('removeFromS3Dir', async () => {
    mockS3ListObjects.mockImplementation(() => ({
      promise: () => Promise.resolve({ Contents: [{ Key: 'file1' }, { Key: 'file2' }] }),
    }));
    mockS3DeleteObjects.mockImplementation(() => ({
      promise: () => Promise.resolve(),
    }));

    await s3.removeFromS3Dir('dir1');

    expect(mockS3ListObjects).toBeCalledWith({
      Bucket: 'bucket',
      Prefix: 'dir1',
    });
    expect(mockS3DeleteObjects).toBeCalledWith({
      Bucket: 'bucket',
      Delete: {
        Objects: [{ Key: 'file1' }, { Key: 'file2' }],
        Quiet: false,
      },
    });
  });

  test('removeFromS3File', async () => {
    mockS3DeleteObjects.mockImplementation(() => ({
      promise: () => Promise.resolve(),
    }));

    await s3.removeFromS3File('dir1/file1');

    expect(mockS3DeleteObjects).toBeCalledWith({
      Bucket: 'bucket',
      Delete: {
        Objects: [{ Key: 'dir1/file1' }],
        Quiet: false,
      },
    });
  });

  test('downloadFromS3Dir success', async () => {
    mockS3ListObjects.mockImplementation(() => ({
      promise: () => Promise.resolve({ Contents: [{ Key: 'file1' }] }),
    }));

    const writeStream = new PassThrough();

    mockS3GetObject.mockImplementation(() => ({
      createReadStream: () => Readable.from(['bin-data']),
    }));

    jest.spyOn(fs, 'ensureDir').mockReturnValue({} as never);
    jest.spyOn(path, 'resolve').mockReturnValue('resolvePath');
    jest.spyOn(path, 'basename').mockReturnValue('basename');

    const mockCreateWriteStream = jest
      .spyOn(fs, 'createWriteStream')
      .mockReturnValue((writeStream as any) as WriteStream);

    await s3.downloadFromS3Dir('dir/files', 'c:/mydir');

    expect(mockCreateWriteStream).toBeCalledWith('resolvePath');
    expect(mockS3GetObject).toBeCalledWith({
      Bucket: 'bucket',
      Key: 'file1',
    });
  });

  test('downloadFromS3Dir, read-stream finished with error', async () => {
    mockS3ListObjects.mockImplementation(() => ({
      promise: () => Promise.resolve({ Contents: [{ Key: 'file1' }] }),
    }));

    const writeStream = new PassThrough();

    mockS3GetObject.mockImplementation(() => ({
      createReadStream: () =>
        Readable.from({
          [Symbol.iterator]: function*() {
            yield new Error();
          },
        }),
    }));

    jest.spyOn(fs, 'ensureDir').mockReturnValue({} as never);
    jest.spyOn(path, 'resolve').mockReturnValue('resolvePath');
    jest.spyOn(path, 'basename').mockReturnValue('basename');

    const mockCreateWriteStream = jest
      .spyOn(fs, 'createWriteStream')
      .mockReturnValue((writeStream as any) as WriteStream);

    try {
      await s3.downloadFromS3Dir('dir/files', 'c:/mydir');

      expect(1).toBe('never happened');
    } catch (e) {
      expect(mockCreateWriteStream).toBeCalledWith('resolvePath');
      expect(mockS3GetObject).toBeCalledWith({
        Bucket: 'bucket',
        Key: 'file1',
      });
    }
  });

  test('downloadFromS3Dir, write-stream finished with error', async () => {
    mockS3ListObjects.mockImplementation(() => ({
      promise: () => Promise.resolve({ Contents: [{ Key: 'file1' }] }),
    }));

    const writeStream = new PassThrough();
    const readStream = Readable.from(['bin-data']);

    mockS3GetObject.mockImplementation(() => ({
      createReadStream: () => readStream,
    }));

    jest.spyOn(fs, 'ensureDir').mockReturnValue({} as never);
    jest.spyOn(path, 'resolve').mockReturnValue('resolvePath');
    jest.spyOn(path, 'basename').mockReturnValue('basename');
    jest.spyOn(writeStream, 'end').mockImplementation(() => {
      writeStream.emit('error', new Error('io'));
    });

    const mockCreateWriteStream = jest
      .spyOn(fs, 'createWriteStream')
      .mockReturnValue((writeStream as any) as WriteStream);

    try {
      await s3.downloadFromS3Dir('dir/files', 'c:/mydir');

      expect(1).toBe('never happened');
    } catch (e) {
      expect(mockCreateWriteStream).toBeCalledWith('resolvePath');
      expect(mockS3GetObject).toBeCalledWith({
        Bucket: 'bucket',
        Key: 'file1',
      });
    }
  });
});
