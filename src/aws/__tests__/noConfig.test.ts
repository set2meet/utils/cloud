import s3cConfig from '../s3cConfig';
import sqsConfig from '../sqsConfig';

jest.mock('config', () => {
  const has = jest.fn().mockReturnValue(false);

  return {
    has,
  };
});

test('test s3c config is not defined', () => {
  expect(s3cConfig).toBeNull();
});

test('test sqs config is not defined', () => {
  expect(sqsConfig).toBeNull();
});
