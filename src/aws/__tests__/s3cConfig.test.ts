import _get from 'lodash/get';
import s3cConfig from '../s3cConfig';

jest.mock('config', () => {
  const cfg = {
    s3c: {
      credentials: {
        key: 'x',
        secret: 'x',
      },
      maxRetries: 1,
      destination: {
        endpoint: 'http://localhost:4566',
        bucket: 's2m-app-recording',
        target: {
          draft: 'dev-dump',
          final: 'dev-output',
        },
      },
    },
  };

  const has = jest.fn().mockImplementation((name: string) => !!_get(cfg, name));
  const get = jest.fn().mockImplementation((name) => _get(cfg, name) as string);

  return {
    has,
    get,
  };
});

test('test s3c config exists', () => {
  expect(s3cConfig).toBeDefined();
  expect(s3cConfig.maxRetries).toBe(1);
  expect(s3cConfig.credentials).toEqual({
    key: 'x',
    secret: 'x',
  });
  expect(s3cConfig.destination).toEqual({
    endpoint: 'http://localhost:4566',
    bucket: 's2m-app-recording',
    target: {
      draft: 'dev-dump',
      final: 'dev-output',
    },
  });
});
