import cfg from 'config';
import { S3Target } from '../types';

const baseKey = 's3c';

export type TS3BConfig = {
  credentials: {
    key: string;
    secret: string;
  };
  maxRetries?: number;
  destination: {
    bucket: string;
    endpoint?: string;
    target: {
      [S3Target.DRAFT]: string;
      [S3Target.FINAL]: string;
    };
  };
};

const configExists =
  cfg.has(baseKey) &&
  cfg.has(`${baseKey}.credentials`) &&
  cfg.has(`${baseKey}.credentials.key`) &&
  cfg.has(`${baseKey}.credentials.secret`) &&
  cfg.has(`${baseKey}.destination`) &&
  cfg.has(`${baseKey}.destination.bucket`) &&
  cfg.has(`${baseKey}.destination.target`) &&
  cfg.has(`${baseKey}.destination.target.${S3Target.DRAFT}`) &&
  cfg.has(`${baseKey}.destination.target.${S3Target.FINAL}`);

const config: TS3BConfig = configExists
  ? {
      credentials: {
        key: cfg.get(`${baseKey}.credentials.key`),
        secret: cfg.get(`${baseKey}.credentials.secret`),
      },
      destination: {
        bucket: cfg.get(`${baseKey}.destination.bucket`),
        target: {
          [S3Target.DRAFT]: cfg.get(`${baseKey}.destination.target.${S3Target.DRAFT}`),
          [S3Target.FINAL]: cfg.get(`${baseKey}.destination.target.${S3Target.FINAL}`),
        },
      },
    }
  : null;

if (config && cfg.has(`${baseKey}.destination.endpoint`)) {
  config.destination.endpoint = cfg.get(`${baseKey}.destination.endpoint`);
}

if (config && cfg.has(`${baseKey}.maxRetries`)) {
  config.maxRetries = cfg.get(`${baseKey}.maxRetries`);
}

export default config;
