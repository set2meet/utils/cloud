import awsStorageConfig from './aws/s3cConfig';
import S3Storage from './aws/S3Storage';
import { IStorage } from './types';

let storage: IStorage = null;

const getStorage = (): IStorage => {
  if (awsStorageConfig) {
    storage = new S3Storage();
  }

  if (storage) {
    return storage;
  }

  throw new Error('Cloud Storage is not configured.');
};

export default getStorage;
