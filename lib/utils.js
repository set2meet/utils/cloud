"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.safeParseJSON = void 0;
exports.safeParseJSON = (data) => {
    return JSON.parse(data);
};
