export declare type TSQSConfig = {
  credentials: {
    key: string;
    secret: string;
  };
  maxRetries?: number;
  destination: {
    group: string;
    region: string;
    queue: {
      task: string;
      report: string;
    };
  };
};
declare const config: TSQSConfig;
export default config;
