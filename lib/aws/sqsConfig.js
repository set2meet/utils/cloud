"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("config"));
const baseKey = 'sqs';
const configExists = config_1.default.has(baseKey) &&
    config_1.default.has(`${baseKey}.credentials`) &&
    config_1.default.has(`${baseKey}.credentials.key`) &&
    config_1.default.has(`${baseKey}.credentials.secret`) &&
    config_1.default.has(`${baseKey}.destination`) &&
    config_1.default.has(`${baseKey}.destination.group`) &&
    config_1.default.has(`${baseKey}.destination.region`) &&
    config_1.default.has(`${baseKey}.destination.queue`) &&
    config_1.default.has(`${baseKey}.destination.queue.task`) &&
    config_1.default.has(`${baseKey}.destination.queue.report`);
const config = configExists
    ? {
        credentials: {
            key: config_1.default.get(`${baseKey}.credentials.key`),
            secret: config_1.default.get(`${baseKey}.credentials.secret`),
        },
        destination: {
            group: config_1.default.get(`${baseKey}.destination.group`),
            region: config_1.default.get(`${baseKey}.destination.region`),
            queue: {
                task: config_1.default.get(`${baseKey}.destination.queue.task`),
                report: config_1.default.get(`${baseKey}.destination.queue.report`),
            },
        },
    }
    : null;
if (config && config_1.default.has(`${baseKey}.maxRetries`)) {
    config.maxRetries = config_1.default.get(`${baseKey}.maxRetries`);
}
exports.default = config;
