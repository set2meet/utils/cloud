import { TMetadata, IStorage } from '../types';
export default class S3Storage implements IStorage {
  private downloadFile;
  private downloadListOfFiles;
  private removeFiles;
  private getListOfFiles;
  private uploadFile;
  uploadRecordingCaptureVideos(id: string, src: string, metadata?: TMetadata): Promise<string>;
  private s3bucket;
  constructor();
  signedUrl(key: string): Promise<string>;
  uploadRecordingProcessingVideo(src: string, metadata?: TMetadata): Promise<string>;
  removeFromS3Dir(src: string): Promise<void>;
  removeFromS3File(src: string): Promise<void>;
  downloadFromS3Dir(src: string, dst: string): Promise<void>;
}
