import Consumer from '../Consumer';
import { TConsumerConfig, TTaskBody, IQueue } from '../types';
export default class SQSQueue implements IQueue {
  private sqs;
  private sendMessage;
  private receiveMessage;
  private removeMessage;
  private changeMessageVisibility;
  constructor();
  createRecordTask(task: TTaskBody): Promise<void>;
  createRecordReport(task: TTaskBody): Promise<void>;
  getConsumerForTask(consumerConfig: TConsumerConfig): Consumer;
  getConsumerForReport(consumerConfig: TConsumerConfig): Consumer;
}
