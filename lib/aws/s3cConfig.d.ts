import { S3Target } from '../types';
export declare type TS3BConfig = {
  credentials: {
    key: string;
    secret: string;
  };
  maxRetries?: number;
  destination: {
    bucket: string;
    endpoint?: string;
    target: {
      [S3Target.DRAFT]: string;
      [S3Target.FINAL]: string;
    };
  };
};
declare const config: TS3BConfig;
export default config;
