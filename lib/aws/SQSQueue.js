"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const uuid_1 = __importDefault(require("uuid"));
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const sqsConfig_1 = __importDefault(require("./sqsConfig"));
const Consumer_1 = __importDefault(require("../Consumer"));
const logger_1 = require("../logger/logger");
const isFifoQueue = (url) => url.endsWith('.fifo');
class SQSQueue {
    constructor() {
        this.sendMessage = (url, task) => __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().sqs.info('Send message', { url, task });
            const specificParams = isFifoQueue(url)
                ? {
                    MessageGroupId: sqsConfig_1.default.destination.group,
                    MessageDeduplicationId: uuid_1.default(),
                }
                : {};
            yield this.sqs
                .sendMessage(Object.assign({ QueueUrl: url, MessageBody: JSON.stringify(task) }, specificParams))
                .promise();
        });
        this.receiveMessage = (url, visibilityTimeout) => __awaiter(this, void 0, void 0, function* () {
            const resp = yield this.sqs
                .receiveMessage({
                QueueUrl: url,
                MaxNumberOfMessages: 1,
                VisibilityTimeout: visibilityTimeout,
            })
                .promise();
            const messages = resp.Messages || [];
            const message = messages[0];
            if (!message) {
                return null;
            }
            logger_1.getLogger().sqs.info('Receive message', { url, message });
            return {
                id: message.ReceiptHandle,
                body: message.Body,
                receivedAt: Date.now(),
            };
        });
        this.removeMessage = (url, id) => __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().sqs.info('Remove message', { url, id });
            yield this.sqs
                .deleteMessage({
                QueueUrl: url,
                ReceiptHandle: id,
            })
                .promise();
        });
        this.changeMessageVisibility = (url, id, lifetime) => __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().sqs.info('Change message visibility', { url, id, lifetime });
            yield this.sqs
                .changeMessageVisibility({
                QueueUrl: url,
                ReceiptHandle: id,
                VisibilityTimeout: lifetime,
            })
                .promise();
        });
        this.sqs = new aws_sdk_1.default.SQS({
            region: sqsConfig_1.default.destination.region,
            accessKeyId: sqsConfig_1.default.credentials.key,
            secretAccessKey: sqsConfig_1.default.credentials.secret,
            maxRetries: sqsConfig_1.default.maxRetries,
        });
    }
    createRecordTask(task) {
        const taskUrl = sqsConfig_1.default.destination.queue.task;
        return this.sendMessage(taskUrl, task);
    }
    createRecordReport(task) {
        const reportUrl = sqsConfig_1.default.destination.queue.report;
        return this.sendMessage(reportUrl, task);
    }
    getConsumerForTask(consumerConfig) {
        const taskUrl = sqsConfig_1.default.destination.queue.task;
        return new Consumer_1.default(consumerConfig, {
            get: (visibilityTimeout) => this.receiveMessage(taskUrl, visibilityTimeout),
            del: (id) => this.removeMessage(taskUrl, id),
            wait: (id, lifetime) => this.changeMessageVisibility(taskUrl, id, lifetime),
        });
    }
    getConsumerForReport(consumerConfig) {
        const reportUrl = sqsConfig_1.default.destination.queue.report;
        return new Consumer_1.default(consumerConfig, {
            get: (visibilityTimeout) => this.receiveMessage(reportUrl, visibilityTimeout),
            del: (id) => this.removeMessage(reportUrl, id),
            wait: (id, lifetime) => this.changeMessageVisibility(reportUrl, id, lifetime),
        });
    }
}
exports.default = SQSQueue;
