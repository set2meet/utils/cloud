"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const config_1 = __importDefault(require("config"));
const types_1 = require("../types");
const baseKey = 's3c';
const configExists = config_1.default.has(baseKey) &&
    config_1.default.has(`${baseKey}.credentials`) &&
    config_1.default.has(`${baseKey}.credentials.key`) &&
    config_1.default.has(`${baseKey}.credentials.secret`) &&
    config_1.default.has(`${baseKey}.destination`) &&
    config_1.default.has(`${baseKey}.destination.bucket`) &&
    config_1.default.has(`${baseKey}.destination.target`) &&
    config_1.default.has(`${baseKey}.destination.target.${types_1.S3Target.DRAFT}`) &&
    config_1.default.has(`${baseKey}.destination.target.${types_1.S3Target.FINAL}`);
const config = configExists
    ? {
        credentials: {
            key: config_1.default.get(`${baseKey}.credentials.key`),
            secret: config_1.default.get(`${baseKey}.credentials.secret`),
        },
        destination: {
            bucket: config_1.default.get(`${baseKey}.destination.bucket`),
            target: {
                [types_1.S3Target.DRAFT]: config_1.default.get(`${baseKey}.destination.target.${types_1.S3Target.DRAFT}`),
                [types_1.S3Target.FINAL]: config_1.default.get(`${baseKey}.destination.target.${types_1.S3Target.FINAL}`),
            },
        },
    }
    : null;
if (config && config_1.default.has(`${baseKey}.destination.endpoint`)) {
    config.destination.endpoint = config_1.default.get(`${baseKey}.destination.endpoint`);
}
if (config && config_1.default.has(`${baseKey}.maxRetries`)) {
    config.maxRetries = config_1.default.get(`${baseKey}.maxRetries`);
}
exports.default = config;
