"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_extra_1 = __importDefault(require("fs-extra"));
const aws_sdk_1 = __importDefault(require("aws-sdk"));
const s3cConfig_1 = __importDefault(require("./s3cConfig"));
const dateformat_1 = __importDefault(require("dateformat"));
const types_1 = require("../types");
const logger_1 = require("../logger/logger");
const ELogErrorCode_1 = require("../logger/ELogErrorCode");
const dateFormatPath = () => dateformat_1.default('yyyy/mm/dd');
const dateFormatPrefix = () => dateformat_1.default('yyyy-mm-dd');
const getDestDir = (id, target) => {
    return `${s3cConfig_1.default.destination.target[target]}/${dateFormatPrefix()}...${id}`;
};
const buildMapFullPath = (dst, src, metadata) => {
    return (name) => ({
        src: path_1.default.resolve(src, name),
        dst: `${dst}/${name}`,
        metadata,
    });
};
class S3Storage {
    constructor() {
        this.downloadFile = (dst) => (src) => {
            return new Promise((resolve, reject) => {
                const dstFilePath = path_1.default.resolve(dst, path_1.default.basename(src));
                const writeStream = fs_extra_1.default.createWriteStream(dstFilePath);
                logger_1.getLogger().s3.info('downloadFile start', { src, dst: dstFilePath });
                const s3stream = this.s3bucket
                    .getObject({
                    Bucket: s3cConfig_1.default.destination.bucket,
                    Key: src,
                })
                    .createReadStream();
                // listen for errors returned by the service
                s3stream.on('error', (err) => {
                    logger_1.getLogger().s3.error(ELogErrorCode_1.ELogErrorCode.S3Operation, 'downloadFile read stream error', { message: err.message });
                    reject(err);
                });
                // pipe s3 read stream to local path
                s3stream
                    .pipe(writeStream)
                    .on('error', (err) => {
                    logger_1.getLogger().s3.error(ELogErrorCode_1.ELogErrorCode.S3Operation, 'downloadFile write stream error', { message: err.message });
                    logger_1.getLogger().s3.info('downloadFile start', { src, dst: dstFilePath });
                    reject(err);
                })
                    .on('finish', () => {
                    logger_1.getLogger().s3.info('downloadFile finish');
                    resolve();
                });
            });
        };
        this.downloadListOfFiles = (dst, list) => __awaiter(this, void 0, void 0, function* () {
            return yield Promise.all(list.map(this.downloadFile(dst)));
        });
        this.removeFiles = (files) => __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().s3.info('removeFiles', { files });
            const params = {
                Bucket: s3cConfig_1.default.destination.bucket,
                Delete: {
                    Quiet: false,
                    Objects: files.map((Key) => ({ Key })),
                },
            };
            yield this.s3bucket.deleteObjects(params).promise();
        });
        this.getListOfFiles = (src) => __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().s3.info('getListOfFiles', { src });
            const data = yield this.s3bucket
                .listObjects({
                Bucket: s3cConfig_1.default.destination.bucket,
                Prefix: src,
            })
                .promise();
            return data.Contents.map((item) => item.Key);
        });
        this.uploadFile = (file) => {
            const readStream = fs_extra_1.default.createReadStream(file.src);
            const params = {
                Bucket: s3cConfig_1.default.destination.bucket,
                Body: readStream,
                Key: file.dst,
                Metadata: file.metadata,
            };
            logger_1.getLogger().s3.info('uploadFile', { file });
            return this.s3bucket.upload(params).promise();
        };
        const endpoint = s3cConfig_1.default.destination.endpoint;
        this.s3bucket = new aws_sdk_1.default.S3({
            endpoint,
            s3ForcePathStyle: !!endpoint,
            accessKeyId: s3cConfig_1.default.credentials.key,
            secretAccessKey: s3cConfig_1.default.credentials.secret,
            maxRetries: s3cConfig_1.default.maxRetries,
        });
    }
    uploadRecordingCaptureVideos(id, src, metadata) {
        return __awaiter(this, void 0, void 0, function* () {
            const dst = getDestDir(id, types_1.S3Target.DRAFT);
            const mapToFileUpload = buildMapFullPath(dst, src, metadata);
            const items = yield fs_extra_1.default.readdir(src);
            logger_1.getLogger().s3.info('uploadRecordingCaptureVideos', {
                src,
                dst,
                count: items.length,
            });
            if (items.length) {
                const files = items.map(mapToFileUpload);
                yield Promise.all(files.map(this.uploadFile));
            }
            return dst;
        });
    }
    signedUrl(key) {
        return __awaiter(this, void 0, void 0, function* () {
            const signedUrlExpireSeconds = 300;
            const params = {
                Bucket: s3cConfig_1.default.destination.bucket,
                Key: key,
            };
            yield this.s3bucket.headObject(params).promise();
            return yield this.s3bucket.getSignedUrlPromise('getObject', Object.assign(Object.assign({}, params), { Expires: signedUrlExpireSeconds }));
        });
    }
    uploadRecordingProcessingVideo(src, metadata) {
        return __awaiter(this, void 0, void 0, function* () {
            const filename = path_1.default.basename(src);
            const dst = `${s3cConfig_1.default.destination.target[types_1.S3Target.FINAL]}/${dateFormatPath()}/${filename}`;
            logger_1.getLogger().s3.info('uploadRecordingProcessingVideo', { src, dst });
            yield this.uploadFile({ src, dst, metadata });
            return dst;
        });
    }
    removeFromS3Dir(src) {
        return __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().s3.info('removeFromS3Dir', { src });
            const list = yield this.getListOfFiles(src);
            yield this.removeFiles(list);
        });
    }
    removeFromS3File(src) {
        return __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().s3.info('removeFromS3File', { src });
            yield this.removeFiles([src]);
        });
    }
    downloadFromS3Dir(src, dst) {
        return __awaiter(this, void 0, void 0, function* () {
            logger_1.getLogger().s3.info('downloadFromS3Dir', { src, dst });
            yield fs_extra_1.default.ensureDir(dst);
            const list = yield this.getListOfFiles(src);
            yield this.downloadListOfFiles(dst, list);
        });
    }
}
exports.default = S3Storage;
