"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = __importDefault(require("@s2m/logger"));
const queue = [];
const entity = {
    log: (event) => {
        queue.push(event);
    },
};
void logger_1.default().then((logger) => {
    entity.log = (event) => {
        logger.logEvent(event);
    };
    queue.forEach((event) => {
        entity.log(event);
    });
    queue.length = 0;
});
exports.default = (prefix) => (event) => {
    // add name prefix to event name property, if exists
    // to make it easier to analyze events from
    if (prefix) {
        event.name = `[${prefix}] ${event.name}`;
    }
    entity.log(event);
};
