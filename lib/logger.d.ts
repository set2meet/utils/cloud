export declare type TEvent = {
  name: string;
  [key: string]: unknown;
};
declare const _default: (prefix?: string) => (event: TEvent) => void;
export default _default;
