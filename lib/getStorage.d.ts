import { IStorage } from './types';
declare const getStorage: () => IStorage;
export default getStorage;
