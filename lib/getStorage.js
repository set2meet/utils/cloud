"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const s3cConfig_1 = __importDefault(require("./aws/s3cConfig"));
const S3Storage_1 = __importDefault(require("./aws/S3Storage"));
let storage = null;
const getStorage = () => {
    if (s3cConfig_1.default) {
        storage = new S3Storage_1.default();
    }
    if (storage) {
        return storage;
    }
    throw new Error('Cloud Storage is not configured.');
};
exports.default = getStorage;
