"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sqsConfig_1 = __importDefault(require("./aws/sqsConfig"));
const SQSQueue_1 = __importDefault(require("./aws/SQSQueue"));
let queue = null;
const getQueue = () => {
    if (sqsConfig_1.default) {
        queue = new SQSQueue_1.default();
    }
    if (queue) {
        return queue;
    }
    throw new Error('Cloud Queue is not configured.');
};
exports.default = getQueue;
