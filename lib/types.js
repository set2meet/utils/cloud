"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.S3Target = exports.TaskActionType = void 0;
var TaskActionType;
(function (TaskActionType) {
    TaskActionType["Create"] = "create";
    TaskActionType["Remove"] = "remove";
})(TaskActionType = exports.TaskActionType || (exports.TaskActionType = {}));
var S3Target;
(function (S3Target) {
    S3Target["DRAFT"] = "draft";
    S3Target["FINAL"] = "final";
})(S3Target = exports.S3Target || (exports.S3Target = {}));
