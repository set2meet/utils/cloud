/// <reference types="node" />
import { EventEmitter } from 'events';
import { TConsumerConfig, TTaskBody, TQueueTaskManager, TTaskListener, IConsumer } from './types';
export default class Consumer extends EventEmitter implements IConsumer {
  private queueTaskManager;
  private taskDescription;
  private nextTaskTimeout;
  private searchTaskTimeout;
  /**
   * time on which postpone current task in sec
   */
  private waitTimeout;
  /**
   * time on which current task will be invisible for consumers in sec
   */
  private visibilityTimeout;
  constructor(config: TConsumerConfig, queueTaskManager: TQueueTaskManager);
  private nextTask;
  private taskBreak;
  private taskDone;
  emitTask(taskBody: TTaskBody): void;
  onTask(listener: TTaskListener): void;
  private searchNewTask;
  private getNewTaskDescription;
  private terminateTaskTimeout;
}
