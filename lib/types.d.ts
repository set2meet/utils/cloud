export declare type TTaskBodyPropertyValue = number | string;
export declare enum TaskActionType {
  Create = 'create',
  Remove = 'remove',
}
export declare type TTaskListener = (task: TQueueTask) => void;
export declare type TConsumerConfig = {
  nextTaskTimeout: number;
  searchTaskTimeout: number;
  waitTimeout?: number;
  visibilityTimeout?: number;
};
export declare type TTaskBody = {
  id: string;
  src: string;
  action: TaskActionType;
  [key: string]: TTaskBodyPropertyValue;
};
export declare type TQueueTask = {
  desc: TTaskBody;
  done: () => Promise<void>;
  break: () => Promise<void>;
};
export interface IConsumer {
  emitTask(taskBody: TTaskBody): void;
  onTask(listener: TTaskListener): void;
}
export interface IQueue {
  createRecordTask(task: TTaskBody): Promise<void>;
  createRecordReport(task: TTaskBody): Promise<void>;
  getConsumerForTask(consumerConfig: TConsumerConfig): IConsumer;
  getConsumerForReport(consumerConfig: TConsumerConfig): IConsumer;
}
export declare enum S3Target {
  DRAFT = 'draft',
  FINAL = 'final',
}
export declare type TTaskDescription = {
  id: string;
  body: TTaskBody;
  receivedAt: number;
};
export declare type TQueueMessage = {
  id: string;
  body: string;
  receivedAt: number;
};
export declare type TQueueTaskGet = (visibilityTimeoutSeconds: number) => Promise<TQueueMessage>;
export declare type TQueueTaskDel = (id: string) => Promise<void>;
export declare type TQueueTaskWait = (id: string, seconds: number) => Promise<void>;
export declare type TQueueTaskManager = {
  get: TQueueTaskGet;
  del: TQueueTaskDel;
  wait: TQueueTaskWait;
};
export declare type TMetadata = {
  type?: string;
};
export interface IStorage {
  uploadRecordingCaptureVideos(id: string, src: string): Promise<string>;
  uploadRecordingProcessingVideo(src: string, metadata?: TMetadata): Promise<string>;
  signedUrl(key: string): Promise<string>;
  removeFromS3Dir(src: string): Promise<void>;
  removeFromS3File(src: string): Promise<void>;
  downloadFromS3Dir(src: string, dst: string): Promise<void>;
}
