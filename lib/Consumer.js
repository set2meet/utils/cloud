"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const events_1 = require("events");
const utils_1 = require("./utils");
const oneSec = 1000;
const brokenTaskTimeout = 600; // 10 min in seconds
const defaultVisibilityTimeout = 600; // 10 min in seconds
class Consumer extends events_1.EventEmitter {
    constructor(config, queueTaskManager) {
        super();
        this.nextTask = () => {
            // eslint-disable-next-line @typescript-eslint/no-misused-promises
            setTimeout(this.searchNewTask, this.nextTaskTimeout);
        };
        this.taskBreak = () => __awaiter(this, void 0, void 0, function* () {
            yield this.terminateTaskTimeout();
            this.nextTask();
        });
        this.taskDone = () => __awaiter(this, void 0, void 0, function* () {
            yield this.queueTaskManager.del(this.taskDescription.id);
            this.nextTask();
        });
        this.searchNewTask = () => __awaiter(this, void 0, void 0, function* () {
            this.taskDescription = yield this.getNewTaskDescription();
            if (this.taskDescription) {
                this.emitTask(this.taskDescription.body);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-misused-promises
                setTimeout(this.searchNewTask, this.searchTaskTimeout);
            }
        });
        this.getNewTaskDescription = () => __awaiter(this, void 0, void 0, function* () {
            const message = yield this.queueTaskManager.get(this.visibilityTimeout);
            if (!message) {
                return null;
            }
            const body = utils_1.safeParseJSON(message.body);
            return Object.assign(Object.assign({}, message), { body });
        });
        this.queueTaskManager = queueTaskManager;
        this.nextTaskTimeout = config.nextTaskTimeout;
        this.searchTaskTimeout = config.searchTaskTimeout;
        this.waitTimeout = config.waitTimeout || brokenTaskTimeout;
        this.visibilityTimeout = config.visibilityTimeout || defaultVisibilityTimeout;
        this.nextTask();
    }
    emitTask(taskBody) {
        this.emit('task', {
            desc: taskBody,
            done: this.taskDone,
            break: this.taskBreak,
        });
    }
    onTask(listener) {
        this.on('task', listener);
    }
    terminateTaskTimeout() {
        return __awaiter(this, void 0, void 0, function* () {
            const lifetime = Math.round((Date.now() - this.taskDescription.receivedAt) / oneSec);
            yield this.queueTaskManager.wait(this.taskDescription.id, lifetime + this.waitTimeout);
        });
    }
}
exports.default = Consumer;
