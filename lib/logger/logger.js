"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getLogger = exports.registerLogger = void 0;
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
const logger_1 = require("@s2m/logger");
const ELogModule_1 = require("./ELogModule");
let cumulativeLogger;
let parentLogger = new logger_1.DummyLogger();
const initLoggers = (logger) => {
    cumulativeLogger = {
        sqs: logger.createBoundChild(ELogModule_1.ELogModule.Sqs),
        s3: logger.createBoundChild(ELogModule_1.ELogModule.S3),
    };
};
initLoggers(parentLogger);
exports.registerLogger = (logger) => {
    parentLogger = logger;
    initLoggers(parentLogger);
};
exports.getLogger = () => {
    return cumulativeLogger;
};
