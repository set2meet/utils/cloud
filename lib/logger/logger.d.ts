import { ILogger } from '@s2m/logger';
import { ELogErrorCode } from './ELogErrorCode';
import { ELogModule } from './ELogModule';
interface ICumulativeLogger {
  sqs: ILogger<ELogModule, ELogErrorCode>;
  s3: ILogger<ELogModule, ELogErrorCode>;
}
export declare const registerLogger: (logger: ILogger<ELogModule, ELogErrorCode>) => void;
export declare const getLogger: () => ICumulativeLogger;
export {};
