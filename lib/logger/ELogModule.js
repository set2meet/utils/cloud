"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ELogModule = void 0;
var ELogModule;
(function (ELogModule) {
    ELogModule["Sqs"] = "Sqs";
    ELogModule["S3"] = "S3";
})(ELogModule = exports.ELogModule || (exports.ELogModule = {}));
